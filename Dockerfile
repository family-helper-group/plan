FROM openjdk:8-alpine

MAINTAINER Mansur
ENV VERSION="1.0.0"

RUN mkdir /app
WORKDIR /app
COPY build/libs/plan-$VERSION.jar /app
VOLUME /app

EXPOSE 8081

ENTRYPOINT java -Dspring.profiles.active=prod -jar plan-$VERSION.jar